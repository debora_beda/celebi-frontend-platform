import React from 'react';
import Template from '../../components/template/Template'
import AppTab from '../../components/app-tab/AppTab'
import './App.css';

function App() {
  return (
    <div className='App-Container'>
      <Template title='Módulos Ativos'>
        <AppTab></AppTab>
      </Template>
    </div>
  );
}

export default App;
