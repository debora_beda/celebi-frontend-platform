import React from 'react';
import Template from '../../components/template/Template'

import './Monitoramento-Barragem.css';

function MonitoramentoBarragem() {
  return (
    <div className='Monitoramento-Barragem'>
      <Template 
        title='Monitoramento de Barragem'
      >
      </Template>
    </div>
  );
}

export default MonitoramentoBarragem;
