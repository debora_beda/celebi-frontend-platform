import React from 'react';
import Template from '../../components/template/Template'

import './Cadastro-Ativos.css';

function CadastroAtivos() {
  return (
    <div className='Cadastro-Ativos'>
      <Template 
        title='Cadastro de Ativos' 
        buttonText='Cadastrar Novo Ativo' 
        buttonLink='/novo-ativo'
      >
      </Template>
    </div>
  );
}

export default CadastroAtivos;
