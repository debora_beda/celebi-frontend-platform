import React from 'react';
import Template from '../../components/template/Template'

import './Seguranca-Comunicacao.css';

function SegurancaComunicacao() {
  return (
    <div className='Seguranca-Camunicacao'>
      <Template 
        title='Segurança e Comunicação' 
        buttonText='Enviar alerta' 
        buttonLink='/'
      >
      </Template>
    </div>
  );
}

export default SegurancaComunicacao;
