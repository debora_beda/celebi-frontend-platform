import React from 'react';
import Template from '../../components/template/Template'
import NovoAtivoForm from '../../components/form/Novo-Ativo'

import './Novo-Ativo.css';

function NovoAtivo() {
  return (
    <div className='Novo-Ativo'>
      <Template 
        title='Cadastrar Novo Ativo' 
        buttonText='Voltar' 
        buttonLink='/cadastro-ativos'
      >
        <NovoAtivoForm></NovoAtivoForm>
      </Template>
    </div>
  );
}

export default NovoAtivo;
