import React from 'react';
import ReactDOM from 'react-dom';
import App from './pages/app/App';
import CadastroAtivos from './pages/cadastro-ativos/Cadastro-Ativos'
import NovoAtivo from './pages/novo-ativo/Novo-Ativo'
import SegurancaComunicacao from './pages/seguranca-comunicacao/Seguranca-Comunicacao'
import MonitoramentoBarragem from './pages/monitoramento-barragem/Monitoramento-Barragem'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import './index.css';

ReactDOM.render(
  <BrowserRouter>
    <Switch>
        <Route path="/" exact={true} component={App} />
        <Route path="/cadastro-ativos" exact={true} component={CadastroAtivos} />
        <Route path="/novo-ativo" exact={true} component={NovoAtivo} />
        <Route path="/seguranca-comunicacao" exact={true} component={SegurancaComunicacao} />
        <Route path="/monitoramento-barragem" exact={true} component={MonitoramentoBarragem} />
    </Switch>
  </ BrowserRouter>,
  document.getElementById('root')
);
