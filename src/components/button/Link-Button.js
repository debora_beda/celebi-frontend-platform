import React from 'react';
import { Link } from 'react-router-dom'

import './Button.css';

function LinkButton(props) {
  return (
  <Link to={props.link} className='Link-Button'>{props.text}</Link>
  );
}

export default LinkButton;