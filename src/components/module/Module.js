import React from 'react';
import { Link } from 'react-router-dom'
import './Module.css';

function Module(props) {
  return (
    <div className='Module'>
        <Link to={props.link} className='Module-icon'>
          <img src={props.icon} alt={props.alt} className='icon'/>
        </Link>
        <h3>{props.name}</h3>
    </div>
  );
}

export default Module;