import React, { Fragment, useState, useEffect } from 'react';
import Header from '../header/Header'
import LinkButton from '../button/Link-Button';
import Tab from '../tab/Tab';

import './Template.css';

function Template(props) {
  const [button, renderButton] = useState(false);

  useEffect(() => {
    renderButton(props.buttonText != null);
  }, []);

  return (
    <div className='Template'>
      <Header></Header>
      <main>
        <div className='header'>
          <h2>{props.title}</h2>
          {
            (button && <LinkButton text={props.buttonText} link={props.buttonLink}></LinkButton>) || 
            <Fragment></Fragment>
          }
        </div>
        <Tab>
          {props.children}
        </Tab>
      </main>
    </div>
  );
}

export default Template;
