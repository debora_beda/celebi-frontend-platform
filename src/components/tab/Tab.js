import React from 'react';
import './Tab.css';

function Tab(props) {
  return (
    <section className='Tab'>
      {props.children}
    </section>
  );
}

export default Tab;