import React from 'react';
import { Link } from 'react-router-dom'
import logo from '../../img/plant.svg';
import './Header.css';

function Header() {
  return (
      <header className='App-header'>
        <div className='header-container'> 
          <div className='header-logo'>
            <img src={logo} alt='Celebi Logo' className='logo'/>
          </div>
          <Link to="/" className='link'><h1>Celebi</h1></Link>
        </div>
      </header>
  );
}

export default Header;
