import React from 'react';
import Module from '../module/Module'

import './AppTab.css';

import SafetyIcon from '../../img/safety.svg'
import PainelIcon from '../../img/painel.svg'
import ToolsIcon from '../../img/tools.svg'

function AppTab() {
  return (
      <ul className='App-Tab'>
        <li>
          <Module 
            name="Cadastro de ativos" 
            link='/cadastro-ativos'
            icon={ToolsIcon} 
            alt='Imagem de Ferramentas - Fonte: Ícones feitos por <a href="https://smashicons.com/" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/br/" title="Flaticon"> www.flaticon.com</a>'>
          </Module></li>
        <li>
          <Module 
            name="Monitoramento de Barragens" 
            link='/monitoramento-barragem'
            icon={PainelIcon} 
            alt='Imagem de Tela com gráficos - Fonte: Ícones feitos por <a href="https://www.flaticon.com/br/autores/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/br/" title="Flaticon"> www.flaticon.com</a>'>
          </Module></li>
        <li>
          <Module 
            name="Segurança e Comunicação" 
            link='/seguranca-comunicacao' 
            icon={SafetyIcon} 
            alt='Imagem de Escudo - Fonte: Ícones feitos por <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/br/" title="Flaticon"> www.flaticon.com</a>'>
          </Module>
          </li>
      </ul>  
  );
}

export default AppTab;