import React from 'react';
import { Formik, Field, Form } from 'formik';
import './Novo-Ativo.css';

function NovoAtivoForm() {

  function getDate() {
    const today = new Date();
    const day = (today.getDate() < 10 ? `0${today.getDate()}`: today.getDate())
    const month = (today.getMonth() < 9 ? `0${today.getMonth()+1}`: today.getMonth()+1)
    return `${day}/${month}/${today.getFullYear()}`;
  };

  return (
    <div className="Cadastro-Novo-Ativo">
      <Formik
      initialValues={{
        codigo: '',
        tipo: '',
        subtipo: '',
        descricao: '',
        dataEntrada: getDate(),
      }}
      onSubmit={async values => {
        await new Promise(r => setTimeout(r, 500));
        console.log(JSON.stringify(values, null, 2));
      }}
    >
      <Form>
        <label htmlFor="codigo">Código</label>
        <Field id="codigo" name="codigo" placeholder="" className='field'/>

        <label htmlFor="tipo">Tipo</label>
        <Field id="tipo" name="tipo" placeholder="Ferramenta" className='field' />

        <label htmlFor="subtipo">Subtipo</label>
        <Field id="subtipo" name="subtipo" placeholder="Martelo" className='field' />

        <label htmlFor="descricao">Descrição</label>
        <Field id="descricao" name="descricao" placeholder="" className='field descricao' />
        
        <label htmlFor="dataEntrada">Data de Entrada</label>
        <Field id="dataEntrada" name="dataEntrada" placeholder="" className='field' />

        <button type="submit">Cadastrar</button>
      </Form>
    </Formik>
    </div>
  );
}

export default NovoAtivoForm;
